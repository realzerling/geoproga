package com.example.geoproga;

public class MarkerSpisok {
    String drawableName;
    Double  lattitude;
    Double longitude;

    public MarkerSpisok(String drawableName, Double lattitude, Double longitude) {
        this.drawableName = drawableName;
        this.lattitude = lattitude;
        this.longitude = longitude;
    }

    public MarkerSpisok() {

    }

    public String getDrawableName() {
        return drawableName;
    }

    public void setDrawableName(String drawableName) {
        this.drawableName = drawableName;
    }

    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
