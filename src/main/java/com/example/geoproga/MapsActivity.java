package com.example.geoproga;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    /* List<MarkerOptions> markerList;*/
    List<MarkerSpisok>spisokMarkerov;

    boolean dalirazreshenie;
    FusedLocationProviderClient fusedLocationProviderClient;/*hren*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
         spisokMarkerov = new ArrayList<MarkerSpisok>();



        spisokMarkerov.add(new MarkerSpisok("classic.png",55.838000,60.599100 ));
        spisokMarkerov.add(new MarkerSpisok("ham.png",55.838001,60.599100 ));


        //List<MarkerOptions> markerList = new ArrayList<MarkerOptions>();
       // markerList.add(new MarkerOptions().position(new LatLng(55.838026, 60.597115)));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);/*кнопки зума включает*/
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[] {
                            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
                    },
                    1);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);

            }

//            Task locationResult = fusedLocationProviderClient.getLastLocation();    /* запуск ЗАДАЧИ которая выдаст последнии координаты телефона*/
//            locationResult.addOnCompleteListener(this, new OnCompleteListener() {
//                @Override
//                public void onComplete(@NonNull Task task) {
//                    if (task.isComplete()) {
//                        Location lastlacation = (Location) task.getResult(); /*в скобках локатион это приведение типа*/
//                        LatLng mylocation = new LatLng(lastlacation.getLatitude(), lastlacation.getLongitude());
//                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mylocation, 17));//и передвинет камеру на эту локацию
//
//                    }
//                }
//            });


            for (int i=0;i<spisokMarkerov.size();i++)
            {
                mMap.addMarker(new MarkerOptions().position(new LatLng(spisokMarkerov.get(i).getLattitude( ),spisokMarkerov.get(i).getLongitude()))
                .icon(BitmapDescriptorFactory.fromBitmap(resizeIconka("cheese",100,100))));

            }
            LatLng sydney = new LatLng(56.838026, 60.597115);
            /* mMap.addMarker(markerList.get(0));*/
            /*mMap.addMarker(new MarkerOptions().position(sydney).title("Pizza 1905").alpha(0.7f).icon(BitmapDescriptorFactory.fromResource(R.drawable.cheese)));*/
       /* mMap.addMarker(new MarkerOptions().position(sydney).title("Pizza 1905").alpha(0.7f)
                .snippet("Wow ! Pizza!").draggable(false).visible(true));*/
            mMap.addMarker(new MarkerOptions().position(sydney).alpha(0.7f).icon(BitmapDescriptorFactory.fromBitmap(resizeIconka("cheese",100,100))));
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    Toast.makeText(getApplicationContext(), "ya kliknul na markere", Toast.LENGTH_LONG).show();
                    return false;
                }
            });
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,17));
        }
    }
    public Bitmap resizeIconka(String drawableName,int shirina, int visota ) {
        Bitmap image = BitmapFactory.decodeResource(getResources(),
                                    getResources().getIdentifier(drawableName,
                                    "drawable",getPackageName()));
        return Bitmap.createScaledBitmap(image,shirina,visota,false);
    }


}